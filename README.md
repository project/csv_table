<pre>
  ┌───────┐
  │       │
  │  a:o  │  acolono.com
  │       │
  └───────┘
</pre>

CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------

Provides a Block which uses a CSV as source for building a Table whose results can be filtered via a input field.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/csv_table

* To submit bug reports and feature suggestions, or track changes:
  https://www.drupal.org/project/issues/csv_table?status=All&categories=All

REQUIREMENTS
------------

This module has no special requirements.


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

Place the block to a theme region. The block config has the following options:

* Mode (local file / remote URL)
* Show/hide the table header
* Display table search input field
* Sticky header on/off
* Inline CSS `height` property value
* Inline CSS `max-height` property value
* Use default CSS (you can use your own CSS instead)

MAINTAINERS
-----------

Current maintainers:
* Nikolay Grachev (granik) - https://www.drupal.org/u/granik /developed by
* Nico Grienauer (grienauer) - https://www.drupal.org/u/grienauer


by acolono GmbH
---------------

~~we build your websites~~
we build your business

hello@acolono.com

www.acolono.com
www.twitter.com/acolono
www.drupal.org/acolono-gmbh
