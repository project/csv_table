<?php

namespace Drupal\csv_table;

/**
 * Interface describing helper class.
 */
interface CsvTableHelperInterface {

  /**
   * Get CSV file by its path.
   *
   * @param string $path
   *   Path to CSV file.
   *
   * @return string|false
   *   CSV contents or FALSE if file not found.
   */
  public function getCsvByPath(string $path);

  /**
   * Get remote CSV file.
   *
   * @param string $url
   *   Remote file URL.
   *
   * @return mixed
   *   CSV contents or FALSE in case of error.
   */
  public function getRemoteFileContents($url);

  /**
   * Get source CSV string.
   *
   * @param array $config
   *   Block Configuration.
   *
   * @return string|null
   *   CSV contents as string.
   */
  public function getCsvString(array $config);

  /**
   * Returns a Form API table element, with an executable header.
   *
   * @param array $config
   *   Block configuration array.
   *
   * @return array|null
   *   Table element render array.
   */
  public function buildTable(array $config);

  /**
   * Decode the CSV into an Array no matter via which method it is received.
   *
   * @param array $config
   *   Block configuration.
   *
   * @return array|mixed
   *   CSV contents as array.
   */
  public function getCsvArray(array $config);

  /**
   * Make some actions if remote file is updated.
   *
   * @param array $config
   *   Block configuration.
   * @param int|null $lastmod
   *   Last known file modification timestamp.
   *
   * @return bool
   *   Flag if remote file has been updated.
   */
  public function processRemoteFileUpdates(array $config, $lastmod);

  /**
   * Get cache tags names for block.
   *
   * @param array $config
   *   CSV table block config.
   *
   * @return array
   *   Cache tags.
   */
  public function getCacheTagNames(array $config);

}
